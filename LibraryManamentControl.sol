// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

// Access Restriction
contract LibraryManamentControl {
    address public owner;
    address public libarian;

    constructor() {
        owner = msg.sender;
    }

    modifier onlyLibarian() {
        require(msg.sender == libarian);
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function appointLibarian(address _libAddress) public onlyOwner {
        libarian = _libAddress;
    }

    function setUpLibary() public onlyLibarian {}
}
