// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

// State machine pattern
contract StateMachine {
    enum Stages {
        AcceptingBlindBids,
        RevealBids,
        WinnerDetermined,
        Finished
    }

    Stages public stage = Stages.AcceptingBlindBids;

    uint256 public creationTime = block.timestamp;

    modifier atStage(Stages _stage) {
        require(stage == _stage);
        _;
    }

    modifier transitionAfter() {
        _;
        nextStage();
    }

    modifier timedTransitions() {
        if (
            stage == Stages.AcceptingBlindBids &&
            block.timestamp >= creationTime + 6 days
        ) {
            nextStage();
        }
        if (
            stage == Stages.RevealBids &&
            block.timestamp >= creationTime + 10 days
        ) {
            nextStage();
        }
        _;
    }

    function bid()
        public
        payable
        timedTransitions
        atStage(Stages.AcceptingBlindBids)
    {}

    function reveal() public timedTransitions atStage(Stages.RevealBids) {}

    function claimGoods()
        public
        timedTransitions
        atStage(Stages.WinnerDetermined)
        transitionAfter
    {}

    function cleanup() public atStage(Stages.Finished) {}

    function nextStage() internal {
        stage = Stages(uint256(stage) + 1);
    }
}
