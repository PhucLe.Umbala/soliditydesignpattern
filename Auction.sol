// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

// Check Effect Interaction Pattern.
contract Auction {
    address payable public beneficiary = payable(msg.sender);
    address highestBidder;
    uint256 highestBid;
    uint256 public auctionEndDuration = block.timestamp + 3 days;
    bool ended;
    mapping(address => uint256) refunds;

    function bid() public payable {
        require(msg.value >= highestBid);
        if (highestBidder != address(0)) {
            refunds[highestBidder] += highestBid; // record the refund that this user can claim
        }
        highestBidder = msg.sender;
        highestBid = msg.value;
    }

    function withdrawRefund() public {
        uint256 refund = refunds[msg.sender];
        refunds[msg.sender] = 0;
        payable(msg.sender).transfer(refund);
    }

    function auctionEnd() public {
        // 1. Checks
        require(block.timestamp >= auctionEndDuration);
        require(!ended);
        // 2. Effects
        ended = true;
        // 3. Interaction
        beneficiary.transfer(highestBid);
    }
}
