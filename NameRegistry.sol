// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract NameRegistry {
    struct ContractDetails {
        address owner;
        address contractAddress;
        uint16 version;
    }
    mapping(string => ContractDetails) registry;

    function registerName(
        string calldata _name,
        address addr,
        uint16 ver
    ) public returns (bool) {
        // versions should start from 1
        require(ver >= 1);

        ContractDetails memory info = registry[_name];
        require(info.owner == msg.sender);
        // create info if it doesn't exist in the registry
        if (info.contractAddress == address(0)) {
            info = ContractDetails({
                owner: msg.sender,
                contractAddress: addr,
                version: ver
            });
        } else {
            info.version = ver;
            info.contractAddress = addr;
        }
        // update record in the registry
        registry[_name] = info;
        return true;
    }

    function getContractDetails(string calldata _name)
        public
        view
        returns (address, uint16)
    {
        return (registry[_name].contractAddress, registry[_name].version);
    }
}
