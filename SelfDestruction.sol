// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

// Contract Self Destruction 
contract SelfDesctructionContract {
    address payable owner;
    string public someValue;

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    // constructor
    constructor() {
        owner = payable(msg.sender);
    }

    function setSomeValue(string memory value) public {
        someValue = value;
    }

    function destroyContract() public onlyOwner {
        selfdestruct(owner);
    }
}
