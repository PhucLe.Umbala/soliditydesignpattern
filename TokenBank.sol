// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

// Withdraw design pattern
contract TokenBank {
    address owner;
    address[] internal investors;
    mapping(address => uint256) internal balances;

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "not the owner");
        _;
    }

    function registerInvestor(address _investor) public onlyOwner {
        require(_investor != address(0), "invalid investor's address");
        investors.push(_investor);
    }

    function calculateDividendAccured(address _investor)
        internal
        returns (uint256)
    {}

    // bad
    function distributeDividends() public onlyOwner {
        for (uint256 i = 0; i < investors.length; i++) {
            uint256 amount = calculateDividendAccured(investors[i]);
            payable(investors[i]).transfer(amount); // push ether to address
        }
    }

    // good
    function claimDividend() public {
        uint256 amount = calculateDividendAccured(msg.sender);
        require(amount > 0, "no dividends to claim");
        balances[msg.sender] = 0;
        payable(msg.sender).transfer(amount); // pull ether from the contract
    }
}
