// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

// Emergency stop pattern 
contract Bank {
    address payable public owner;
    bool paused = false;

    constructor() {
        owner = payable(msg.sender);
    }

    modifier onlyOwner() {
        require(payable(msg.sender) == owner);
        _;
    }

    modifier isPaused() {
        require(paused);
        _;
    }

    modifier notPause() {
        require(!paused);
        _;
    }

    function pauseContract() public onlyOwner notPause {
        paused = true;
    }

    function unpauseContract() public onlyOwner isPaused {
        paused = false;
    }

    function depositEther() public notPause {}
}
